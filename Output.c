#include "Output.h"
//#include "cbw.h"

static unsigned int OutputImage;

/*Eine Prozedur applyOutputToProcess, die den Inhalt des intern erstellte 
 Prozessabbild für die Aktuatoren an die Ausgabe-Ports schreibt und damit die 
 Aktuatoren der Anlage aktiviert/deaktiviert*/
void applyOutputToProcess() {
    unsigned char PortA = 0;
    unsigned char PortCL = 0;
    PortA = (char) OutputImage;
    PortCL = OutputImage >> 8;
    //cbDOut(0, FIRSTPORTA, PortA);
    //cbDOut(0, FIRSTPORTCL, PortCL);
}

/*Die Funktion setBItInOutput setzt im Prozessabbild für Aktuatoren die als 
Bit-Maske übergebenen Bit*/
void setBitInOutput(int bitOutputMask) {
    OutputImage = OutputImage | bitOutputMask;
    return;
}

/*Die Funktion clearBitInOutput löscht im Prozessabbild für Aktuatoren die als
Bit-Maske übergebenen Bit*/
void clearBitInOutput(int bitOutputMask) {
    OutputImage = OutputImage & ~bitOutputMask;
    return;
}

/*Die Funktion resetOutputs setzt im Prozessabbild alle Aktuatoren auf einen
 sicheren Wert und überträgt das Prozessabbild an die Ausgabe-Ports*/
void resetOutputs() {
    OutputImage = restore;
}

