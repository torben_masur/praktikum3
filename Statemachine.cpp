#include "Statemachine.h"

#include "Input.h"
#include "Output.h"
#include <time.h>
#include "Verwaltungsklasse.h"


using namespace std;

enum states {
    initState, initStateStartPressed, state2, state2StartPressed, transStart, messung, messungOk, messungFehler,
    weiche, weicheKeinMetall, weicheMetall, rutsche, auslauf, nullState
};

static enum states aktuellerZustand = initState;
static enum states timeState = nullState;
static int timeOfTheState = 0;

void evalStatemachine() {

    static Verwaltungsklasse currentdata;
    
    unsigned int bitOutputMask = 0;
    unsigned int bitOutputClearMask = 0;

    unsigned int bitInputMask = 0;

    switch (aktuellerZustand) {
        case initState:
            bitOutputClearMask = WEICHEAUF | AMPELGRUEN | AMPELGELB | AMPELROT | LEDQ1 | LEDQ2;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | LEDSTARTTASTE;
            setBitInOutput(bitOutputMask);
            break;
        case initStateStartPressed:

            bitOutputClearMask = WEICHEAUF | AMPELGRUEN | AMPELGELB | AMPELROT | LEDQ1 | LEDQ2;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | LEDSTARTTASTE;
            setBitInOutput(bitOutputMask);
            break;
        case state2:

            bitOutputClearMask = AMPELROT;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = LEDSTARTTASTE | MOTORSTOP | AMPELGRUEN;
            setBitInOutput(bitOutputMask);
            break;
        case state2StartPressed:

            bitOutputClearMask = LEDSTARTTASTE;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | AMPELGRUEN;
            setBitInOutput(bitOutputMask);
            break;
        case transStart:

            bitOutputClearMask =LEDSTARTTASTE | AMPELGRUEN | MOTORSTOP | LEDQ1;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORRECHTSLAUF | AMPELGELB;
            setBitInOutput(bitOutputMask);
            break;
        case messung:

            bitOutputClearMask = MOTORRECHTSLAUF | AMPELGELB;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;
        case messungOk:

            bitOutputClearMask = MOTORSTOP | AMPELROT;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORRECHTSLAUF | MOTORLANGSAM | AMPELGRUEN | LEDQ1;
            setBitInOutput(bitOutputMask);
            break;
        case messungFehler:

            bitOutputClearMask = MOTORSTOP;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORRECHTSLAUF | MOTORLANGSAM | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;

        case weiche:

            bitOutputClearMask = MOTORLANGSAM | MOTORRECHTSLAUF | AMPELGRUEN | AMPELROT;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;
            
        case weicheKeinMetall:

            bitOutputClearMask = MOTORSTOP | AMPELROT;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORRECHTSLAUF | AMPELGELB | WEICHEAUF | LEDQ2;
            setBitInOutput(bitOutputMask);
            break;
            
        case weicheMetall:

            bitOutputClearMask = MOTORSTOP;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORRECHTSLAUF | AMPELGELB | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;
            
        case auslauf:

            bitOutputClearMask = MOTORRECHTSLAUF | AMPELGELB | WEICHEAUF | LEDQ2;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | AMPELGRUEN;
            setBitInOutput(bitOutputMask);
            break;
            
        case rutsche:

            bitOutputClearMask = MOTORRECHTSLAUF | AMPELGELB | AMPELROT | LEDQ1 | LEDQ2;
            clearBitInOutput(bitOutputClearMask);

            bitOutputMask = MOTORSTOP | AMPELROT;
            setBitInOutput(bitOutputMask);
            break;
    }
    switch (aktuellerZustand) {

        case initState:
            bitInputMask = TASTESTART | KEINEINLAUFWERKSTUECK | KEINWERKSTUECKINHOEHENMESSUNG | KEINWERKSTUECKINWEICHE | RUTSCHELEER | KEINWERKSTUECKIMAUSLAUF | TASTESTOP | ESTOP;
            if (isBitMaskSet(bitInputMask)) {
                aktuellerZustand = state2StartPressed;
                
                currentdata.ListeLeeren();
                
            }
            break;
            
        case initStateStartPressed:
            if (stateTime() >= 1 && !isBitSet(TASTESTART)) {
                aktuellerZustand = initState;
            } 
            break;
            
        case state2:
            if (!isBitSet(KEINEINLAUFWERKSTUECK) && isBitSet(RUTSCHELEER)) {
                aktuellerZustand = transStart;
                
                currentdata.PutNewObjektOfMeasurementInList();
                currentdata.SetIteratorToLastObjekt();
                currentdata.SetStartzeit();
                
            } else if (isBitSet(TASTESTART)) {
                aktuellerZustand = initStateStartPressed;
                
                currentdata.GesamtzahlAufKonsoleAusgeben();
                
                currentdata.ListeInDateiAusgeben();

            } 
            break;

        case state2StartPressed:
            if (!isBitSet(KEINEINLAUFWERKSTUECK) && isBitSet(RUTSCHELEER)) {
                aktuellerZustand = transStart;
                
                currentdata.PutNewObjektOfMeasurementInList();
                currentdata.SetIteratorToLastObjekt();
                currentdata.SetStartzeit();
                
            } else if (stateTime() > 1 && !isBitSet(TASTESTART)) {
                aktuellerZustand = state2;
                
            } 
            break;

        case transStart:
            if (!isBitSet(KEINWERKSTUECKINHOEHENMESSUNG)) {
                aktuellerZustand = messung;
            } 
            break;

        case messung:
            if (isBitSet(WERKSTUECKHOEHEOK)) {
                aktuellerZustand = messungOk;
                
                currentdata.setHoeheOK(true);
                
            } else if (!isBitSet(WERKSTUECKHOEHEOK)) {
                aktuellerZustand = messungFehler;
                
                currentdata.setHoeheOK(false);
                
            } 
            break;

        case messungOk:
            if (!isBitSet(KEINWERKSTUECKINWEICHE)) {
                aktuellerZustand = weiche;
                
            } 
            break;
            
        case messungFehler:
            if (!isBitSet(KEINWERKSTUECKINWEICHE)) {
                aktuellerZustand = weiche;
                
            } 
            break;

        case weiche:
            if (isBitSet(WERKSTUECKMETALL)) {
                aktuellerZustand = weicheMetall;
                
                currentdata.setIsMetall(true);
                
            } else if (!isBitSet(WERKSTUECKMETALL)) {
                aktuellerZustand = weicheKeinMetall;
                
                currentdata.setIsMetall(false);
                
            }
            break;

        case weicheMetall:
            if (!isBitSet(RUTSCHELEER)) {
                aktuellerZustand = rutsche;
            } 
            break;
            
        case weicheKeinMetall:

            if (!isBitSet(KEINWERKSTUECKIMAUSLAUF)) {
                aktuellerZustand = auslauf;
            }
            break;
            
        case rutsche:

            if (!isBitSet(RUTSCHELEER) && isBitSet(KEINWERKSTUECKINWEICHE)) {
                aktuellerZustand = state2;
                
                currentdata.setEndzeit();
                
            } 
            break;
        case auslauf:

            if (isBitSet(KEINWERKSTUECKIMAUSLAUF)) {
                aktuellerZustand = state2;
                                
                currentdata.setEndzeit();
                
            } 
    }
}

int stateTime() {

    int var;

    // Wird der Zustand gewechselt, wird die Zeit auf Null gesetzt
    if (aktuellerZustand != timeState) {
        timeOfTheState = time(NULL);
        timeState = aktuellerZustand;
    }

    var = (time(NULL) - timeOfTheState);
    return var;

}