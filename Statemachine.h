#ifndef STATEMACHINE_H
#define	STATEMACHINE_H
#define STOP 0

#include <vector>
#include "measurements.h"

void evalStatemachine();
int stateTime();

#endif	/* STATEMACHINE_H */