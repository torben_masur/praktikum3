/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Test.cpp
 * Author: torbe
 * 
 * Created on 24. Mai 2016, 10:23
 */

#include "Test.h"
#include "measurements.h"
#include "ti_asssertion.h"
#include <vector>

Test::Test() {
}

Test::Test(const Test& orig) {
}

Test::~Test() {
}

void Test::testStart() {
    // Alle Test in Reinfolge durchlaufen lassen
}
/*
void Test::testMeasurements() {
    // Kriterien zum Testen:
    // Es ist möglich alle Kenndaten in Objekte zu speichern

    // Test der get Funktionen
    measurements TestMeasurement1("eins", true, true, "zwei");

    ASSERT(TestMeasurement1.getStartzeit() == "eins");
    ASSERT(TestMeasurement1.getHoehe() == true);
    ASSERT(TestMeasurement1.getMetall() == true);
    ASSERT(TestMeasurement1.getZeitEnde() == "zwei");

    // Test der set Funktionen
    measurements TestMeasurement2;

    TestMeasurement2.setStartzeit("drei");
    TestMeasurement2.setHoehe(false);
    TestMeasurement2.setMetall(false);
    TestMeasurement2.setZeitEnde("vier");

    ASSERT(TestMeasurement1.getStartzeit() == "drei");
    ASSERT(TestMeasurement1.getHoehe() == false);
    ASSERT(TestMeasurement1.getMetall() == false);
    ASSERT(TestMeasurement1.getZeitEnde() == "vier");

}

void testList() {
    // Kriterien zum Testen:
    // Es können die Objekte in eine Liste gespeichert werden
    vector <measurements> ListOfMeasurements;

    ListOfMeasurements.push_back(Measurements("eins", false, false, "zwei"));
    ListOfMeasurements.push_back(Measurements("baum", true, false, "ast"));
    ListOfMeasurements.push_back(Measurements("haus", false, true, "tuer"));
    ListOfMeasurements.push_back(Measurements("garten", true, true, "gras"));
    ListOfMeasurements.push_back(Measurements("stecker", true, false, "kabel"));

    // Die Objekte aus der Liste mit Assert und get Überprüfen

    ASSERT(ListOfMeasurements[0].getStartzeit() == "eins");
    ASSERT(ListOfMeasurements[0].getHoehe() == false);
    ASSERT(ListOfMeasurements[0].getMetall() == false);
    ASSERT(ListOfMeasurements[0].getZeitEnde() == "zwei");

    ASSERT(ListOfMeasurements[1].getStartzeit() == "baum");
    ASSERT(ListOfMeasurements[1].getHoehe() == true);
    ASSERT(ListOfMeasurements[1].getMetall() == false);
    ASSERT(ListOfMeasurements[1].getZeitEnde() == "ast");

    ASSERT(ListOfMeasurements[2].getStartzeit() == "haus");
    ASSERT(ListOfMeasurements[2].getHoehe() == false);
    ASSERT(ListOfMeasurements[2].getMetall() == true);
    ASSERT(ListOfMeasurements[2].getZeitEnde() == "tuer");

    ASSERT(ListOfMeasurements[3].getStartzeit() == "garten");
    ASSERT(ListOfMeasurements[3].getHoehe() == true);
    ASSERT(ListOfMeasurements[3].getMetall() == true);
    ASSERT(ListOfMeasurements[3].getZeitEnde() == "gras");

    ASSERT(ListOfMeasurements[4].getStartzeit() == "stecker");
    ASSERT(ListOfMeasurements[4].getHoehe() == true);
    ASSERT(ListOfMeasurements[4].getMetall() == false);
    ASSERT(ListOfMeasurements[4].getZeitEnde() == "kabel");

}

void testBildschirmausgabe() {
    // Kriterien zum Testen: 
    // Die Objekte werden auf dem Bildschirm ausgegeben

    measurements Measurement1("eins", false, false, "zwei");
    measurements Measurement2("baum", true, false, "ast");
    measurements Measurement3("haus", false, true, "tuer");
    measurements Measurement4("garten", true, true, "gras");
    measurements Measurement5("stecker", true, false, "kabel");

    Measurement1.werteAufDemBildschirmAusgeben();
    Measurement2.werteAufDemBildschirmAusgeben();
    Measurement3.werteAufDemBildschirmAusgeben();
    Measurement4.werteAufDemBildschirmAusgeben();
    Measurement5.werteAufDemBildschirmAusgeben();

}

void testDateispeicherung() {
    // Kriterien zum Testen:
    // Die Kenndaten werden in Dateien gespeichert

    measurements Measurement1("eins", false, false, "zwei");
    measurements Measurement2("baum", true, false, "ast");
    measurements Measurement3("haus", false, true, "tuer");
    measurements Measurement4("garten", true, true, "gras");
    measurements Measurement5("stecker", true, false, "kabel");

    FILE* datafile;
    datafile = fopen("TestFile", "w");
    
    Measurement1.writeToFile(datafile);
    Measurement2.writeToFile(datafile);
    Measurement3.writeToFile(datafile);
    Measurement4.writeToFile(datafile);
    Measurement5.writeToFile(datafile);
    
    fclose(datafile);

}
*/