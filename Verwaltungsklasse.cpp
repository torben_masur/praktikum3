/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Verwaltungsklasse.cpp
 * Author: torbe
 * 
 * Created on 24. Mai 2016, 17:43
 */

#include "Verwaltungsklasse.h"

Verwaltungsklasse::Verwaltungsklasse() {
}

Verwaltungsklasse::Verwaltungsklasse(const Verwaltungsklasse& orig) {
}

Verwaltungsklasse::~Verwaltungsklasse() {
}

void Verwaltungsklasse::PutNewObjektOfMeasurementInList() {
    // Ein neues Objekt anlegen
    Measurements actualMeasurement;
}

void Verwaltungsklasse::SetIteratorToLastObjekt() {
    currentMeasurement = listOfMeasurements.end() - 1;
}

void Verwaltungsklasse::ListeLeeren(){
    listOfMeasurements.clear();
}

void Verwaltungsklasse::SetStartzeit() {
    currentMeasurement->setStartzeit();
}

void Verwaltungsklasse::setHoeheOK(bool hoeheOK) {
    currentMeasurement->setHoeheOK(hoeheOK);
}

void Verwaltungsklasse::setIsMetall(bool isMetall) {
    currentMeasurement->setIsMetall(isMetall);
}

void Verwaltungsklasse::setEndzeit() {
    currentMeasurement->setEndzeit();
}

void Verwaltungsklasse::GesamtzahlAufKonsoleAusgeben() {
    printf("Gesamtzahl der Werkstücke: %d\n", listOfMeasurements.size());
}

void Verwaltungsklasse::ListeAufBildschirmAusgeben() {
    
    for (std::vector <Measurements>::iterator current = listOfMeasurements.begin(); current != listOfMeasurements.end(); current++) {
        current->werteAufDemBildschirmAusgeben();
    }

}

void Verwaltungsklasse::ListeInDateiAusgeben() {
    
    if (listOfMeasurements.size() != 0) {

        FILE* datafile;
        string dateiName;

        DateinamenGenerieren(dateiName);

        datafile = fopen(dateiName.c_str(), "w");

        for (std::vector <Measurements>::iterator current = listOfMeasurements.begin(); current != listOfMeasurements.end(); current++) {
            current->writeToFile(datafile);
        }
        
        fclose(datafile);
    }
}

void Verwaltungsklasse::DateinamenGenerieren(string &dateiName) {

    dateiName.append("Messdaten Der Werkstuecke ");

    time_t dataTime;
    time(&dataTime);
    string Kennung = ctime(&dataTime);
    
    Kennung.erase(Kennung.end() -1);
    Kennung.replace(13, 1, "-", 0, 1);
    Kennung.replace(16, 1, "-", 0, 1);

    dateiName.append(Kennung);
    dateiName.append(".csv");
}

