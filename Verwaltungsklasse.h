/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Verwaltungsklasse.h
 * Author: torbe
 *
 * Created on 24. Mai 2016, 17:43
 */


#include <vector>
#include "measurements.h"
#include <string>
#include <stdio.h>

#ifndef VERWALTUNGSKLASSE_H
#define VERWALTUNGSKLASSE_H

using namespace std;

class Verwaltungsklasse {
public:
    vector<Measurements> listOfMeasurements;
    vector<Measurements>::iterator currentMeasurement;
    
    Verwaltungsklasse();
    Verwaltungsklasse(const Verwaltungsklasse& orig);
    virtual ~Verwaltungsklasse();
    
    // Objekt erstellen und in die LIste schieben
    // Auf das letzte Objekt aus der Liste zeigen
    // Auf das erste Objekt der Liste zeigen
    // Dinge der Objekte ändern
    // Gesamtzahl der Objekte ausgeben
    // Liste durchlaufen und Bildschirmausgaben vornehmen
    // Liste durchlaufen und Dateiausgaben vornehmen
    // Dateinahmen erzeugen
    
    void PutNewObjektOfMeasurementInList();
    void SetIteratorToLastObjekt();
    void ListeLeeren();
    void SetStartzeit();
    void setHoeheOK(bool hoeheOK);
    void setIsMetall(bool isMetall);
    void setEndzeit();
    void GesamtzahlAufKonsoleAusgeben();
    void ListeAufBildschirmAusgeben();
    void ListeInDateiAusgeben();
    void DateinamenGenerieren(string &dateiName);
  
private:

};

#endif /* VERWALTUNGSKLASSE_H */

