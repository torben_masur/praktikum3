#include "measurements.h"
#include <time.h>
#include <string>
#include <cstring>
#include <stdio.h>
#include <iostream>

using namespace std;

Measurements::Measurements() {
    this->startzeit = 0;
    this->hoeheOK = 0;
    this->isMetall = 0;
    this->endzeit = 0;
}

Measurements::~Measurements() {

}

void Measurements::setStartzeit() {
    time(&startzeit);
}

void Measurements::setHoeheOK(bool hoeheOK) {
    this->hoeheOK = hoeheOK;
}

void Measurements::setIsMetall(bool IsMetall) {
    this->isMetall = IsMetall;
}

void Measurements::setEndzeit() {
    time(&endzeit);
}

bool Measurements::writeToFile(FILE* Ausgabe, char trennung) {

    if (Ausgabe != NULL) {



        string sstartzeit = ctime(&startzeit);
        sstartzeit.erase(sstartzeit.end() - 1);
        string sendzeit = ctime(&endzeit);
        sendzeit.erase(sendzeit.end() - 1);

        fprintf(Ausgabe, "%s %c ", sstartzeit.c_str(), trennung);

        if (hoeheOK) {
            fprintf(Ausgabe, "Hoehe NOT OK %c ");
        } else {
            fprintf(Ausgabe, "Hoehe OK     %c ");
        }

        if (isMetall) {
            fprintf(Ausgabe, "Metall     %c ");
        } else {
            fprintf(Ausgabe, "NOT Metall %c ");
        }

        fprintf(Ausgabe, "%s\n", sendzeit.c_str());
    }

}

void Measurements::werteAufDemBildschirmAusgeben(char trennung) {

    string sstartzeit = ctime(&startzeit);
    sstartzeit.erase(sstartzeit.end() - 1);
    string sendzeit = ctime(&endzeit);
    sendzeit.erase(sendzeit.end() - 1);

    printf("%s %c ", sstartzeit.c_str(), trennung);

    if (hoeheOK) {
        printf("Hoehe NOT OK %c ");
    } else {
        printf("Hoehe OK     %c ");
    }

    if (isMetall) {
        printf("Metall     %c ");
    } else {
        printf("NOT Metall %c ");
    }

    printf("%s\n", sendzeit.c_str());
}