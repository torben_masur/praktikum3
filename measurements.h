/* 
 * File:   measurements.h
 * Author: abz259
 *
 * Created on 13. Mai 2016, 14:41
 */

#define MAXSTRINGLENGTH 30

#include <string>
#include <time.h>


#ifndef MEASUREMENTS_H
#define	MEASUREMENTS_H

using namespace std;

class Measurements {

public:
    time_t startzeit;
    bool hoeheOK;
    bool isMetall;
    time_t endzeit;
    
public:
    Measurements();
    ~Measurements();
    
    bool writeToFile(FILE* Ausgabe, char trennung = ';');
    void werteAufDemBildschirmAusgeben(char trennung = ';');
    
    void setStartzeit();
    void setHoeheOK(bool hoeheOK);
    void setIsMetall(bool IsMetall);
    void setEndzeit();
};



#endif	/* MEASUREMENTS_H */